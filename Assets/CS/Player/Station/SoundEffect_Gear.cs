﻿using UnityEngine;
using System.Collections;

public class SoundEffect_Gear : MonoBehaviour {
    //Plays Sound effects

	void Start () {
	    audio.Play();
	}
	
	void Update () {
	    audio.volume = ((bool)FindSoundPipe ().Sounds ["SFX"]) ? 1 : 0;
        if(!audio.isPlaying){
            GameObject.Destroy(this.gameObject);
        }
	}

    Sound_Pipe FindSoundPipe () {
        GameObject target = GameObject.FindGameObjectWithTag ("Sky");
        return target.GetComponent (typeof(Sound_Pipe)) as Sound_Pipe;
    }
}
