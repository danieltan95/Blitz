﻿using UnityEngine;
using System.Collections;

public class Effects_Pipe : MonoBehaviour {
    //Handles effects playing

    public void playEXP (GameObject explosiontype, Vector3 position) {
        //Plays an explosion
        GameObject particle = explosiontype;
        GameObject.Instantiate (particle, position, Quaternion.identity);
    }
}
