﻿using UnityEngine;
using System.Collections;

public class Explosion_Gear : MonoBehaviour {
    //Plays the explosion

    private ParticleSystem explosion ;

    void Start () {
        explosion = this.GetComponent (typeof(ParticleSystem)) as ParticleSystem;
        explosion.Play ();
    }

    void Update () {
        if (!explosion.isPlaying) {
            GameObject.Destroy (this.gameObject);
        }
    }
}
