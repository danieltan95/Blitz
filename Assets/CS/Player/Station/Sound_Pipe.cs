﻿using UnityEngine;
using System.Collections;

public class Sound_Pipe : MonoBehaviour {
    //Handles sound playing

    public GameObject SFXspeaker;
    public GameObject BGMspeaker;
    public AudioClip[] backgroundmusic;
    public Hashtable Sounds = new Hashtable ();

    // Use this for initialization
    void Start () {
        Sounds ["BGM"] = true;
        Sounds ["SFX"] = true;
        if (GameObject.FindGameObjectWithTag ("Background") == null) {
            playBGM (backgroundmusic);
        }
    }

    public void playSFX (AudioClip clip, Vector3 position) {
        //Creates speaker that have special functions to respond to SFX events
        GameObject speaker = SFXspeaker;
        AudioSource source = speaker.GetComponent (typeof(AudioSource)) as AudioSource;
        source.clip = clip;
        GameObject.Instantiate (speaker, position, Quaternion.identity);
    }

    public void playBGM (AudioClip[] clips) {
        //Creates speaker that have special functions to respond to BGM events
        GameObject speaker = BGMspeaker;
        BackgroundMusic_Gear rgear = speaker.GetComponent (typeof(BackgroundMusic_Gear)) as BackgroundMusic_Gear;
        rgear.cliplist = clips;
        GameObject.Instantiate (speaker);
    }
}
