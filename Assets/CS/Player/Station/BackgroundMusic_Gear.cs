﻿using UnityEngine;
using System.Collections;

public class BackgroundMusic_Gear : MonoBehaviour {
    //Plays the background music

    public AudioClip[] cliplist; //Updated from Sound Pipe
    private bool play = true;

    void Start () {
        GameObject.DontDestroyOnLoad (this.gameObject); //Ensures it will not replay when recreated.
    }

    void Update () {
        audio.volume = ((bool)FindSoundPipe ().Sounds ["BGM"]) ? 1 : 0;
        if (play) {
            play = false;
            StartCoroutine (PlayRandom ());
        }
    }

    Sound_Pipe FindSoundPipe () {
        //Find the Sound Pipe from Sky
        GameObject target = GameObject.FindGameObjectWithTag ("Sky");
        return target.GetComponent (typeof(Sound_Pipe)) as Sound_Pipe;
    }

    IEnumerator PlayRandom () {
        //Shuffles the background song list.
        int soundnow = Random.Range (0, cliplist.Length);
        audio.clip = cliplist [soundnow];
        audio.Play ();
        yield return new WaitForSeconds (audio.clip.length);
        play = true;
    }
}
