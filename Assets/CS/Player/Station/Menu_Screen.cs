﻿using UnityEngine;
using System.Collections;

public class Menu_Screen : MonoBehaviour {
    //Shows the menu. Placeholder until NGUI comes in.

    public GUISkin skin ;
    public string IntroText = "This is a hacked together preview build.\n" +
           "W-Moves forward, press shift to run\n" +
           "S-Brake\n" +
           "A,D-Moves left or right\n" +
           "Q/E-Jumps left or right\n" +
           "Space-Jumps up\n" +
           "C-Switch to first person view, use mouse to rotate camera\n" +
           "V-Switch to travel view, press R/G to rotate camera\n" +
           "F-Switch to weapon view, circles are enemies\n" +
           "X-Switch to normal view\n" +
           "Shoot with right mouse button\n" +
           "Switch weapons with mouse scroll button\n";
    private Hashtable state = new Hashtable ();
    private int volume = 5;
    private Vector2 rotd = new Vector2 (100f, 100f); //For the mouse turn tracer.
    private float sw = Screen.width / 2f;
    private float sh = Screen.height / 2f;


    // Use this for initialization
    void Start () {
        state ["menu"] = false;
        state ["intro"] = false;
        state ["controls"] = false;
        state ["mouse"] = false;
        state ["options"] = false;
        state ["sound"] = false;
        state ["quit"] = false;
    }

    void OnGUI () {
        View ();
    }

    void View () {
        GUI.skin = skin;
        Menu ();
        Intro ();
        Controls ();
        Options ();
        Quit ();
        AudioListener.volume = volume / 10.0f;
    }

    void Menu () {
        var w = 100f;
        var h = 30f;
        GUILayout.BeginArea (new Rect (sw - w * 0.5f, h*0.1f, w, h));
        if (GUILayout.Button ("Menu")) {
            state ["menu"] = true;
        }
        GUILayout.EndArea ();

        if ((bool)state ["menu"]) {
            Time.timeScale = 0f;
            w = 140f;
            h = 200f;
            var rec = new  Rect (sw - w, 50f, w * 2f, h);
            GUI.Box (rec, " ");
            GUILayout.BeginArea (rec);
            if (GUILayout.Button ("Resume")) {
                Time.timeScale = 1f;
                state ["menu"] = false;
            }
            if (GUILayout.Button ("Controls")) {
                state ["controls"] = true;
            }
            if (GUILayout.Button ("Options")) {
                state ["options"] = true;
            }
            if (GUILayout.Button ("Intructions")) {
                state ["intro"] = true;
            }
            if (GUILayout.Button ("Quit")) {
                state ["quit"] = true;
            }
            GUILayout.EndArea ();
        }
    }

    void Intro () {
        if ((bool)state ["intro"]) {
            state ["menu"] = false;
            var w = 140f;
            var h = 300f;
            var rec = new Rect (sw - w, 50f, w * 2f, h);
            GUI.Box (rec, " ");
            GUILayout.BeginArea (rec);
            GUILayout.Label (IntroText);
            if (GUILayout.Button ("Back")) {
                state ["intro"] = false;
                state ["menu"] = true;
            }
            GUILayout.EndArea ();
        }
    }

    void Controls () {
        if ((bool)state ["controls"]) {
            state ["menu"] = false;
            var w = 140f;
            var h = 100f;
            var rec = new Rect (sw - w, 50f, w * 2f, h);
            GUI.Box (rec, " ");
            GUILayout.BeginArea (rec);
            if (GUILayout.Button ("Mouse")) {
                state ["mouse"] = true;
            }
            if (GUILayout.Button ("Back")) {
                state ["controls"] = false;
                state ["menu"] = true;
            }
            GUILayout.EndArea ();
        }
        Mouse ();
    }

    void Mouse () {
        if ((bool)state ["mouse"]) {
            state ["controls"] = false;
            var w = 140f;
            var h = 200f;
            var rec = new Rect (sw - w, 50f, w * 2f, h);
            GUI.Box (rec, " ");
            GUILayout.BeginArea (rec);
            GUILayout.Label ("Rotation per second");
            var cview = Camera.main.GetComponent (typeof(Camera_Screen)) as Camera_Screen;
            rotd.x = cview != null ? ((int)cview.rotationdamping.x) : (int)rotd.x;
            rotd.y = cview != null ? ((int)cview.rotationdamping.y) : (int)rotd.y;
            GUILayout.Label ("X axis " + rotd.x);
            rotd.x = GUILayout.HorizontalSlider (rotd.x, 40f, 120f);
            GUILayout.Label ("Y axis " + rotd.y);
            rotd.y = GUILayout.HorizontalSlider (rotd.y, 40f, 120f);
            if (cview != null) {
                cview.rotationdamping = rotd;
            }
            if (GUILayout.Button ("Back")) {
                state ["mouse"] = false;
                state ["controls"] = true;
            }
            GUILayout.EndArea ();
        }
    }

    void Options () {
        if ((bool)state ["options"]) {
            state ["menu"] = false;
            var w = 140f;
            var h = 100;
            var rec = new Rect (sw - w, 50f, w * 2f, h);
            GUI.Box (rec, " ");
            GUILayout.BeginArea (rec);
            if (GUILayout.Button ("Sound")) {
                state ["sound"] = true;
            }
            if (GUILayout.Button ("Back")) {
                state ["options"] = false;
                state ["menu"] = true;
            }
            GUILayout.EndArea ();
        }
        Sound ();
    }

    void Sound () {
        if ((bool)state ["sound"]) {
            state ["options"] = false;
            var w = 140f;
            var h = 200f;
            var rec = new Rect (sw - w, 50f, w * 2f, h);
            GUI.Box (rec, " ");
            GUILayout.BeginArea (rec);
            GUILayout.Label ("Sound options");
            var shandle = this.gameObject.GetComponent (typeof(Sound_Pipe)) as Sound_Pipe;
            var sta = (bool)shandle.Sounds ["BGM"] ? "on" : "off";
            if (GUILayout.Button ("BGM " + sta)) {
                shandle.Sounds ["BGM"] = ! (bool)shandle.Sounds ["BGM"];
            }
            sta = (bool)shandle.Sounds ["SFX"] ? "on" : "off";
            if (GUILayout.Button ("SFX " + sta)) {
                shandle.Sounds ["SFX"] = ! (bool)shandle.Sounds ["SFX"];
            }
            GUILayout.Label ("Volume " + volume);
            volume = (int)GUILayout.HorizontalSlider (volume, 0, 10);
            if (GUILayout.Button ("Back")) {
                state ["sound"] = false;
                state ["options"] = true;
            }
            GUILayout.EndArea ();
        }
    }

    void Quit () {
        if ((bool)state ["quit"]) {
            state ["menu"] = false;
            Time.timeScale = 1f;
            var w = 140f;
            var h = 100f;
            var rec = new Rect (sw - w, 50f, w * 2f, h);
            GUI.Box (rec, " ");
            GUILayout.BeginArea (rec);
            if (GUILayout.Button ("Return to Start Page")) {
                state ["quit"] = false;
                Application.LoadLevel ("Start_Level");
            }
            if (GUILayout.Button ("Exit Game")) {
                state ["quit"] = false;
                Application.Quit ();
            }
            if (GUILayout.Button ("Back")) {
                state ["quit"] = false;
                state ["menu"] = true;
            }
            GUILayout.EndArea ();
        }
    }
}
