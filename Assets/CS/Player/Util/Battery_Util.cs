﻿using UnityEngine;
using System.Collections;

public class Battery_Util {
    //Custom weapon GUI draw

    public static void DrawCrossHair (GameObject myself, GameObject weapon, ArrayList listofcolliders, Texture crosshair, Texture target) {
        //Find their position on the screen and draw the targeting crosshair on them.
        if (weapon == myself) {
            float wt = Screen.width / 2f;
            float ht = Screen.height / 2f;
            float cw = crosshair.width / 2f;
            float ch = crosshair.height / 2f;
            GUI.Label (new Rect (wt - cw, ht - ch, cw * 2f, ch * 2f), crosshair);
            foreach (var blip in listofcolliders) {
                var position = Camera.main.WorldToScreenPoint ((blip as Transform).position);
                cw = target.width / 2f;
                ch = target.height / 2f;
                GUI.Label (new Rect (position.x - cw, ht * 2f - position.y - ch, cw * 2f, ch * 2f), target);
            }
        }
    }
 
    public static ArrayList ViewedColliders (GameObject myself, float radius) {
        //Returns colliders within viewing range
        ArrayList places = new ArrayList ();
        var SR = myself.transform.root.gameObject;
        var position = myself.transform.position;
        var allhit = Physics.OverlapSphere (position, radius);
        foreach (Collider collider in allhit) {
            var CR = collider.gameObject.transform.root.gameObject;
            if (((CR.GetComponent (typeof(Status_Gear)) as Status_Gear) != null) && CR != SR) {
                var unitvec = CR.transform.position - position;
                //Only draw when the player can see them.
                if (Vector3.Dot (myself.transform.forward, unitvec) >= 0) {
                    if (!places.Contains (CR.transform)) {
                        places.Add (CR.transform);
                    }
                }
            }
        }
        return places;
    }
}
