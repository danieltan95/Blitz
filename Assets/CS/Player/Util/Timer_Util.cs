﻿using UnityEngine;
using System.Collections;

public class Timer_Util {
    //Custom timer class

    private bool start = false;
    public float time = Time.time;

    public bool StartDown (float period) {
        //Do at start of cycle
        bool countdown = false;
        if (start == false) {
            start = true;
            time = Time.time;
            countdown = true;
        } else if (start == true && (Time.time - time) > period) {
            //Reset to default values
            start = false;
            time = Time.time;
        }
        return countdown;
    }

    public bool EndDown (float period) {
        //Do at end of cycle
        bool countdown = false;
        if (start == false) {
            start = true;
            time = Time.time;
        } else if (start == true && (Time.time - time) > period) {
            //Reset to default values
            start = false;
            time = Time.time;
            countdown = true;
        }
        return countdown;
    }

    public bool LoopDown (float period) {
        //Do at loop of cycle
        bool countdown = false;
        if (start == false) {
            start = true;
            time = Time.time;
        } else if (start == true && (Time.time - time) > period) {
            //Reset to default values
            start = false;
            time = Time.time;
        }else{
             countdown=true;
        }
        return countdown;
    }
}
