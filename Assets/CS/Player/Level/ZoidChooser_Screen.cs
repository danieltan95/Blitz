﻿using UnityEngine;
using System.Collections;

public class ZoidChooser_Screen : MonoBehaviour {
    //Enables the player to choose the zoid

    void OnGUI () {
        ArrayList blips = new ArrayList ();
        GameObject SR = this.transform.root.gameObject;
        Vector3 position = this.transform.position;
        //Gets zoids within a radius.
        var allhit = Physics.OverlapSphere (position, 5);
        foreach (Collider collider in allhit) {
            GameObject CR = collider.gameObject.transform.root.gameObject;
            if (CR.CompareTag ("Zoid") && CR != SR) {
                if (!blips.Contains (CR)) {
                    blips.Add (CR);
                }
            }
        }
        if (blips.Count > 0) {
            //Gets the first zoid in that radius.
            string zoid = (blips [0] as GameObject).name;
            string Text = "Press X \n to enter\n " + zoid;
            GUILayout.BeginArea (new Rect (Screen.width * 0.5f - 100f, Screen.height * 0.5f - 50f, 200f, 100f));
            GUILayout.Box (Text);
            GUILayout.EndArea ();
            //Switch to the zoid on player request.
            if (Input.GetKey (KeyCode.X)) {
                var zoidgo=blips [0] as GameObject;
                zoidgo.tag = "Player";
                this.gameObject.tag = "Untagged";
                this.transform.DetachChildren ();//Detach Sky and Camera so they could find the zoid.
                GameObject.Destroy (this.gameObject);
            }
        }
    }
}
