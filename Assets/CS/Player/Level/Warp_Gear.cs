﻿using UnityEngine;
using System.Collections;

public class Warp_Gear : MonoBehaviour {
    //Warps the zoid over to the battlefield.
    private bool flip =true;

    void Start () {
    }
 
    void OnTriggerExit (Collider other) {
        GameObject[] exitlist = GameObject.FindGameObjectsWithTag ("Exit");
        int index = Random.Range (0, exitlist.Length);
        var OR = other.gameObject.transform.root;
        if (OR.gameObject.CompareTag ("Player") && flip==true) {
            flip=false;
            var pos = exitlist [index].transform.position;
            GameObject L = (GameObject)GameObject.Instantiate (OR.gameObject, pos, Quaternion.identity);
            //Consistency between main controller objects.
            string sky = GameObject.FindGameObjectWithTag ("Sky").name;
            if (OR.Find (sky) != null) {
                OR.Find (sky).parent = L.transform;
            }
            //Restore normal position
            (Camera.main.GetComponent (typeof(Camera_Pipe)) as Camera_Pipe).state ["follow"] = "normal";
            GameObject.Destroy (OR.gameObject);
        }
    }
}
