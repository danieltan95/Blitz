﻿using UnityEngine;
using System.Collections;

public class Camera_Screen : MonoBehaviour {
    //Placeholder for custom camera GUI in the future.
    //leng and tip is measure in viewport ratios.

    public Vector3 leng = new Vector3 (0.01f, 0.1f, 0.0f);
    public Vector3 tip = new Vector3 (0.5f, 0.5f, 0.0f);
    public Vector2 rotationdamping = new Vector2 (100f, 100f);

    void Start () {
    }

    void Update () {
    }
}
