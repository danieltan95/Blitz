﻿using UnityEngine;
using System.Collections;

public class HUD_Gear : MonoBehaviour {
    //Stimulate HUD behaviour for each weapon.

    private float rotationDamping = 30.0f;

    void Update () {
        var chandle = this.gameObject.GetComponent (typeof(Camera_Pipe)) as Camera_Pipe;
        var whandle = this.gameObject.transform.root.gameObject.GetComponent (typeof(Weapon_Pipe)) as Weapon_Pipe;
        if (whandle != null) {
            var weapon = whandle.weapon;
            if ((string)chandle.state ["follow"] == "HUD") {
                Follow (weapon);
            }
        }
    }
 
    void Follow (GameObject weapon) {
        //Different weapons have different view points at request.
        var offset = (weapon.GetComponent (typeof(Weapon_Gear)) as Weapon_Gear).offset;
        var position = weapon.transform.localPosition + (new Vector3 (offset [0], offset [1], offset [2]));
        var rotation = weapon.transform.eulerAngles;
        rotation.x += offset [3];
        this.transform.localPosition = Vector3.Lerp (this.transform.localPosition, position, 0.1f);
        this.transform.eulerAngles = Vector3.Lerp (this.transform.eulerAngles, rotation, rotationDamping * Time.deltaTime);
    }
}
