﻿using UnityEngine;
using System.Collections;

public class ThirdPerson_Gear : MonoBehaviour {
    //Third person view mode, for travelling and admiring the zoid body in general.

    public Hashtable state = new Hashtable ();
    public bool flip = false; //To know if the camera has moved to the right position and rotation.
    public float lookdown = 10f;
    private float rotationdamping = 50.0f;

    void Update () {
        var cview = this.gameObject.GetComponent (typeof(Camera_Screen)) as Camera_Screen;
        if (cview != null) {
            rotationdamping = cview.rotationdamping.x;
            var chandle = this.gameObject.GetComponent (typeof(Camera_Pipe)) as Camera_Pipe;
            if ((string)chandle.state ["follow"] == "travel") {
                state = chandle.state;
                Follow ();
            } else {
                flip = false;
            }
        }
    }

    void Follow () {
        //Offset for manipulation during run time as there is no solid way to get the feel
        //Without some experimentation.
        var position = (this.transform.root.gameObject.GetComponent (typeof(Status_Gear))as Status_Gear).offset;
        position.z *= 1.7f; //Moves it backwards to see more of the zoid, at request.
        if ((this.transform.localPosition - position).magnitude < 0.1f) {
            flip = true;
        }
        if (flip == false) {
            this.transform.localPosition = Vector3.Lerp (this.transform.localPosition, position, 0.1f);
            var rotation = this.transform.localEulerAngles;
            rotation.x = lookdown; //Lookdown can be manipulated during run time, also for experimentation.
            this.transform.localEulerAngles = Vector3.Lerp (this.transform.localEulerAngles, rotation, rotationdamping * Time.deltaTime);
        } else {
            if ((string)state ["turn"] == "left") {
                this.transform.RotateAround (this.transform.root.position, Vector3.up, (rotationdamping * Time.deltaTime));
            } else if ((string)state ["turn"] == "right") {
                this.transform.RotateAround (this.transform.root.position, Vector3.up, (-rotationdamping * Time.deltaTime));
            }
        }
    }
}
