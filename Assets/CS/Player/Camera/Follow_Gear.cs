﻿using UnityEngine;
using System.Collections;

public class Follow_Gear : MonoBehaviour {
    //Follow smoothly behind the player.

    public int lookdown = 5;

    void Update () {
        var chandle = this.gameObject.GetComponent (typeof(Camera_Pipe)) as Camera_Pipe;
        var sgear = this.transform.root.gameObject.GetComponent (typeof(Status_Gear)) as Status_Gear;
        if (((string)chandle.state ["follow"]) == "normal") {
            if (sgear != null) {
                Follow (sgear.offset);
            }
        }
    }

    void Follow (Vector3 position) {
        //Using lerp to smooth the distance over time
        this.transform.localPosition = Vector3.Lerp (this.transform.localPosition, position, 0.1f);
        var rotation = this.transform.root.eulerAngles;
        rotation.x+=5; //Look down a bit for see nearer at request.
        this.transform.eulerAngles=rotation;
        //this.transform.eulerAngles = Vector3.Lerp (this.transform.eulerAngles, rotation, 0.3f);
    }

}
