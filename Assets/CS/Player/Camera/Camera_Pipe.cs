﻿using UnityEngine;
using System.Collections;

public class Camera_Pipe : MonoBehaviour {
    //Handles transition between camera modes.

    public Hashtable state = new Hashtable ();
    public float cooltime = 0.1f;
    private Timer_Util record;
    private string original = "normal"; //buffer to check previous state.

    void Start () {
        record = new Timer_Util ();
        state ["follow"] = "normal";
    }

    void Update () {
        //The state is updated from the Player_Pipe
        //The camera returns to the normal position
        //Before changing the mode.
        if ((string)state ["follow"] != original) {
            if (original != "normal") {
                state ["follow"] = "normal";
                original = "normal";
            } else {
                if (record.StartDown (cooltime)) {
                    original = (string)state ["follow"];
                }
            }
        }
    }
}
