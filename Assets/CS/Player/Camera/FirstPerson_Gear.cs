﻿using UnityEngine;
using System.Collections;

public class FirstPerson_Gear : MonoBehaviour {
    //First person view logic for the game.

    public Hashtable state = new Hashtable ();
    public bool flip = false; //to record if the camera has moved to the right position
    public Vector2 arc = new Vector2 (30f, 60f); //Maximum rotation angles.
    private Vector2 rotationdamping = new Vector2 (100f, 100f);

    void Update () {
        var cview = this.gameObject.GetComponent (typeof(Camera_Screen)) as Camera_Screen;
        if (cview != null) {
            //The damping can be altered in the menu, so we need to update it.
            rotationdamping = cview.rotationdamping;
            var chandle = this.gameObject.GetComponent (typeof(Camera_Pipe)) as Camera_Pipe;
            if ((string)chandle.state ["follow"] == "bird") {
                state = chandle.state;
                Follow ();
            } else {
                flip = false;
            }
        }
    }

    float WrapAngle (float angle) {
        //Wraps the angle to a range of -180 to 180
        if (angle > 180) {
            return angle - 360;
        } else {
            return angle;
        }
    }

    void Follow () {
        //The camera position will change for people to zoid, so a custom offset is needed.
        var position = (this.transform.root.gameObject.GetComponent (typeof(Status_Gear))as Status_Gear).offset;
        position.z *= -1; //Moves it forward.
        var rotation = this.transform.root.rotation;
        if ((this.transform.localPosition - position).magnitude < 0.1f) {
            flip = true;
        }
        if (flip == false) {
            //Moves it to the front and points it in front.
            this.transform.localPosition = Vector3.Lerp (this.transform.localPosition, position, 0.1f);
            this.transform.rotation = Quaternion.Lerp (this.transform.rotation, rotation, rotationdamping.x * Time.deltaTime);
        } else {
            //C sharp don't allow direct change to the euler angles.
            var ror = this.transform.localEulerAngles;
            ror.z = 0;
            this.transform.localEulerAngles = ror;
            var angley = WrapAngle (ror.y);
            var anglex = WrapAngle (ror.x);
            if ((string)state ["trackx"] == "left") {
                if (angley > -arc.y) {
                    this.transform.Rotate (Vector3.up * (-rotationdamping.y * Time.deltaTime));
                }
            } else if ((string)state ["trackx"] == "right") {
                if (angley < arc.y) {
                    this.transform.Rotate (Vector3.up * (rotationdamping.y * Time.deltaTime));
                }
            }
            if ((string)state ["tracky"] == "down") {
                if (anglex > -arc.x) {
                    this.transform.Rotate (Vector3.right * (-rotationdamping.x * Time.deltaTime));
                }
            } else if ((string)state ["tracky"] == "up") {
                if (anglex < arc.x) {
                    this.transform.Rotate (Vector3.right * (rotationdamping.x * Time.deltaTime));
                }
            }
        }
    }
}
