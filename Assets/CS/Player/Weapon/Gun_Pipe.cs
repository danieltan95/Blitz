﻿using UnityEngine;
using System.Collections;

public class Gun_Pipe : MonoBehaviour {
    //Enable shooting of bullet

    public bool state  ;
    public float cooltime = 0.5f;
    public GameObject bullet ;
    private Timer_Util record;
    private bool cooldown =false;

    void Start () {
        record = new Timer_Util ();
    }

    void Update () {
        var wmech = this.gameObject.GetComponent (typeof(Weapon_Gear)) as Weapon_Gear;
        state = wmech.state;
        if (state) {
            state = false;
            cooldown=!record.StartDown (cooltime);
            if (!cooldown) {
                var psys = this.gameObject.GetComponent (typeof(ParticleSystem)) as ParticleSystem;
                if (psys != null) {
                    //Plays the shooting animation
                    psys.Play ();
                }
                var bl = GameObject.Instantiate (bullet, this.transform.position, this.transform.rotation) as GameObject;
            }
            wmech.state = state;
        }
        float diff=cooldown ? 1f-(Time.time-record.time)/cooltime :0f;
        wmech.remain = diff>0 ? diff :0;
    }
}
