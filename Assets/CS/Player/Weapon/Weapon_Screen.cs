﻿using UnityEngine;
using System.Collections;

public class Weapon_Screen : MonoBehaviour {
    //Weapons GUI, for the weapon handling.

    public Texture outline;
    private Weapon_Pipe whandle;
    private ArrayList weapons = new ArrayList ();
    private int wpoint = 0;
    private ArrayList wnames = new ArrayList ();

    void Start () {
        whandle = this.gameObject.GetComponent (typeof(Weapon_Pipe)) as Weapon_Pipe;
    }

    void OnGUI () {
        if (this.transform.root.gameObject.CompareTag ("Player")) {
            var sky = GameObject.FindGameObjectWithTag ("Sky").name;
            if (this.transform.root.Find (sky)) {
                GetWeapons ();
                ShowWeapons ();
            }
        }
    }

    void GetWeapons () {
        weapons = whandle.weapons;
        wpoint = whandle.weaponnow;
        wnames = new ArrayList ();
        foreach (var weapon in weapons) {
            var N = ((weapon as GameObject).GetComponent (typeof(Weapon_Gear)) as Weapon_Gear);
            wnames.Add (N);
        }
    }

    void ShowWeapons () {
        float sh = Screen.height / 2f;
        float w = 50f;
        float h = 50f;
        for (int i=0; i<6; i++) {
            //Draw outline if active weapon, else, just black background
            var texture = (i == wpoint) ? outline : null;
            GUI.Box (new Rect (20f, sh * 2f - h * 1.5f - i * h - 2f, w + 4f, h + 4f), texture);
        }
        foreach (Weapon_Gear gear in wnames) {
            //Draws each thumbnail for the weapons
            int ind = wnames.IndexOf (gear);
            GUI.Label (new Rect (25f, sh * 2f - h * 1.5f - ind * h, w, h), gear.texture as Texture);
            if (w * gear.remain > 0) {
                GUI.Box (new Rect (22f, sh * 2f - h * 1.5f - ind * h, w * gear.remain, h), "");
            }
        }
    }
}
