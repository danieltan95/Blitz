﻿using UnityEngine;
using System.Collections;

public class Bullet_Gear : MonoBehaviour {
    // Handles the bullet shooting and damage.

    public Quaternion missrotate;
    public float timeout = 2f;
    public float speed = 100f;
    public float miss = 3f;
    public float damage = 50f;
    public AudioClip shot;
    public AudioClip explosion;
    public GameObject[] explostionprefab;
    private Vector3 unitvec;
    private Timer_Util record;

    // Use this for initialization
    void Start () {
        record = new Timer_Util ();
        FindSoundPipe ().playSFX (shot, this.gameObject.transform.position);
        //Miss fire mechanism
        missrotate = Quaternion.Euler ((float)Random.Range (0, miss),
                                    (float)Random.Range (0, miss),
                                    (float)Random.Range (0, miss)
                                 );
        //Propels the bullet forward
        //Killed the random now
        //unitvec = (missrotate*this.transform.rotation)*Vector3.forward
        unitvec = (this.transform.rotation) * Vector3.forward;
        this.rigidbody.velocity = unitvec * speed;
    }

    // Update is called once per frame
    void Update () {
        OnRayhit ();
        if (record.EndDown (timeout)) {
            FindSoundPipe ().playSFX (explosion, this.transform.position);
            int rand = (int)Random.Range (0, explostionprefab.Length);
            FindEffectsPipe ().playEXP (explostionprefab [rand], this.transform.position);
            GameObject.Destroy (this.gameObject);
        }
    }

    void OnRayhit () {
        //Checks if bullet hits anything
        RaycastHit Hit;
        var SR = this.gameObject.rigidbody;
        if (Physics.Raycast (this.transform.position, unitvec, out Hit, ((SR.velocity.magnitude * Time.deltaTime * 1.2f) - 0.01f))) {
            ;
            Damage (Hit.collider.gameObject);
            FindSoundPipe ().playSFX (explosion, Hit.point);
            int rand = (int)Random.Range (0, explostionprefab.Length);
            FindEffectsPipe ().playEXP (explostionprefab [rand], Hit.point);
            GameObject.Destroy (this.gameObject);
        }
    }

    void Damage (GameObject victim) {
        print (victim.name);
        var dmg = 1.0f;
        //Damage calc
        dmg *= damage;
        //For armour calc
        var smech = victim.GetComponent (typeof(Status_Gear)) as Status_Gear;
        var pmech = victim.GetComponent (typeof(Parts_Gear)) as Parts_Gear;
        if (pmech != null) {
            pmech.health -= dmg;
            print (pmech.health);
        } else if (smech) {
            smech.health -= dmg;
            print (smech.health);
        } else {
            print ("Inanimate object");
        }
    }

    Sound_Pipe FindSoundPipe () {
        GameObject target = GameObject.FindGameObjectWithTag ("Sky");
        return target.GetComponent (typeof(Sound_Pipe)) as Sound_Pipe;
    }

    Effects_Pipe FindEffectsPipe () {
        GameObject target = GameObject.FindGameObjectWithTag ("Sky");
        return target.GetComponent (typeof(Effects_Pipe)) as Effects_Pipe;
    }
}
