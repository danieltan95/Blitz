﻿using UnityEngine;
using System.Collections;

public class Weapon_Gear : MonoBehaviour {
    //Weapon data

    public bool state = false;
    public Texture texture;
    public Vector4 offset = Vector4.zero;
    public float remain=0f; //For GUI timer, the remaining time

}
