﻿using UnityEngine;
using System.Collections;

public class Gun_Screen : MonoBehaviour {
    //GUI for the weapon HUD

    public Texture target ;
    public Texture crosshair;
    private Weapon_Pipe whandle;

    void Start () {
        whandle = this.gameObject.transform.root.gameObject.GetComponent (typeof(Weapon_Pipe)) as Weapon_Pipe;
    }

    void OnGUI () {
        var weapon = whandle.weapon as GameObject;
        if (weapon == this.gameObject) {
            var sky = GameObject.FindGameObjectWithTag ("Sky").name;
            if (this.transform.root.Find (sky)) {
                var chandle = Camera.main.GetComponent (typeof(Camera_Pipe)) as Camera_Pipe;
                if ((string)chandle.state ["follow"] == "HUD") {
                    var blips = Battery_Util.ViewedColliders (this.gameObject, 100.0f);
                    Battery_Util.DrawCrossHair (this.gameObject, weapon, blips, crosshair, target);
                }
            }
        }
    }
}
