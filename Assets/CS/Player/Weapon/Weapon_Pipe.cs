﻿using UnityEngine;
using System.Collections;

public class Weapon_Pipe: MonoBehaviour {
    //Weapons handler, handle shooting events

    public Hashtable state = new Hashtable ();
    //Weapons holder list
    public ArrayList weapons = new ArrayList ();
    //Cycling pointer to current weapon
    public int weaponnow = 0;
    public float cooltime = 0.1f;
    public GameObject weapon;

    void Start () {
        state ["shoot"] = "";
        state ["add"] = "None";
        SearchAllWeapons ();
    }
 
    // Update is called once per frame
    void Update () {
        weapon = (GameObject)weapons [weaponnow];
        if ((string)state ["add"] != "None") {
            if ((string)state ["add"] == "up") {
                weaponnow += 1;
            } else {
                weaponnow -= 1;
            }
            CyclePointer ();
        }
         
        if ((string)state ["shoot"] == "true") {
            state ["shoot"] = false;
            //Sends the signal to the weapon to shoot the bullet
            Weapon_Gear wgear = weapon.GetComponent (typeof(Weapon_Gear)) as Weapon_Gear;
            if (wgear != null) {
                wgear.state = true;
            }
        }
    }

    void SearchAllWeapons () {
        foreach (Transform child in this.gameObject.transform) {
            if (child.tag == "Weapon" && child.gameObject.activeInHierarchy == true) {
                if (!weapons.Contains (child.gameObject)) {
                    weapons.Add (child.gameObject);
                }
            }
        }
    }

    void CyclePointer () {
        if (weaponnow >= weapons.Count) {
            weaponnow = 0;
        } else if (weaponnow < 0) {
            weaponnow = weapons.Count - 1;
        }
    }
}
