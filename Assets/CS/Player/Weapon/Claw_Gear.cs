﻿using UnityEngine;
using System.Collections;

public class Claw_Gear : MonoBehaviour {
    //Enable the Claw melee attack

    public bool state;
    public float cooltime = 0.5f;
    public float timeout = 0.5f;
    public float radius = 5f;
    public float damage = 500f;
    public AudioClip shot;
    public AudioClip explosion;
    private Vector3 unitvec;
    private Timer_Util record;
    private bool cooldown=false;

    void Start () {
        record = new Timer_Util ();
    }

    void Update () {
        var wmech = this.gameObject.GetComponent (typeof(Weapon_Gear)) as Weapon_Gear;
        state = wmech.state;
        if (state) {
            state = false;
            cooldown=!record.StartDown (cooltime);
            if (!cooldown) {
                FindSoundPipe ().playSFX (shot, this.gameObject.transform.position);
                OnSphereHit ();
            }
            wmech.state = state;
        }
        float diff=cooldown ? 1f-(Time.time-record.time)/cooltime :0f;
        wmech.remain = diff>0 ? diff :0;
    }

    void OnSphereHit () {
        print ("Clawing");
        var allhit = Physics.OverlapSphere (this.transform.position, 5);
        var SR = this.gameObject.transform.root.gameObject;
        foreach (Collider collider in allhit) {
            var CR = collider.gameObject.transform.root.gameObject;
            if ((CR.GetComponent (typeof(Status_Gear)) as Status_Gear) && CR != SR) {
                Damage (collider.gameObject);
                FindSoundPipe ().playSFX (explosion, SR.transform.position);
            }
        }
    }

    void Damage (GameObject victim) {
        print (victim.name);
        var dmg = 1.0f;
        //Damage calc
        dmg *= damage;
        //For armour calc
        var smech = victim.GetComponent (typeof(Status_Gear)) as Status_Gear;
        var pmech = victim.GetComponent (typeof(Parts_Gear)) as Parts_Gear;
        if (pmech != null) {
            pmech.health -= dmg;
            print (pmech.health);
        } else if (smech) {
            smech.health -= dmg;
            print (smech.health);
        } else {
            print ("Inanimate object");
        }
    }

    Sound_Pipe FindSoundPipe () {
        GameObject target = GameObject.FindGameObjectWithTag ("Sky");
        return target.GetComponent (typeof(Sound_Pipe)) as Sound_Pipe;
    }
}
