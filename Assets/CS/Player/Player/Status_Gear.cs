﻿using UnityEngine;
using System.Collections;

public class Status_Gear : MonoBehaviour {
    //Handles the health and energy status of the object.
 
    public Hashtable parts = new Hashtable ();
    public float health = 1000000000000000000000.0f;
    public float maxhealth ;
    public float energy = 100.0f;
    //For fitting the camera position
    public Vector3 offset = new Vector3 (0f, 3f, -2.5f);

    void Start () {
        maxhealth = health;
    }

    void Update () {
        //KillSelf () // Taken out on request
        GetHealth();
    }

    void KillSelf () {
        if (health <= 0) {
            GameObject.Destroy (this.gameObject);
        }
    }

    void GetHealth () {
        //Only parts with colliders have the follow script
        Parts_Gear[] Allparts = this.gameObject.GetComponentsInChildren(typeof(Parts_Gear)) as Parts_Gear[];
        if (Allparts !=null) {
            if (Allparts.Length>0) {
                health = 0f;
                foreach (Parts_Gear pmech in Allparts) {
                    health += pmech.health;
                    //Store for GUI use in the future.
                    parts [pmech.gameObject.name] = pmech.health / pmech.maxhealth * 100f;
                }
            }
        }
    }
}
