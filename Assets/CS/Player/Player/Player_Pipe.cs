﻿using UnityEngine;
using System.Collections;

public class Player_Pipe : MonoBehaviour {
    //Handles input from player and pipes to correct scripts.
 
    public GameObject target;
    private Hashtable state = new Hashtable ();

    void Start () {
    }

    void Update () {
        //refreshes state
        state = new Hashtable ();
        target = this.gameObject.transform.root.gameObject;
        if (target != this.gameObject) {
            //Get Motion state
            Motion_Gear mvmech = (Motion_Gear)target.GetComponent (typeof(Motion_Gear));
            if (mvmech != null) {
             
                //Handle boost message
                if (Input.GetKey (KeyCode.Z)) {
                    state ["boost"] = "forward";
                } else {
                    state ["boost"] = "None";
                }
                     
                //Handle motion state
                if (Input.GetKey (KeyCode.W)) {
                    state ["motion"] = "forward";
                } else if (Input.GetKey (KeyCode.S)) {
                    state ["motion"] = "back";
                } else {
                    state ["motion"] = "None";
                }
             
                //Handle running state
                if ((Input.GetKey (KeyCode.LeftShift) || Input.GetKey (KeyCode.RightShift))) {
                    state ["shift"] = "true";
                } else {
                    state ["shift"] = "false";
                }
             
                //Handle turn message
                if (Input.GetKey (KeyCode.A)) {
                    state ["turn"] = "left";
                } else if (Input.GetKey (KeyCode.D)) {
                    state ["turn"] = "right";
                } else {
                    state ["turn"] = "None";
                }
             
                //Handle jump message
                if (Input.GetKey (KeyCode.Space)) {
                    state ["jump"] = "front";
                } else if (Input.GetKey (KeyCode.Q)) {
                    state ["jump"] = "left";
                } else if (Input.GetKey (KeyCode.E)) {
                    state ["jump"] = "right";
                } else {
                    state ["jump"] = "None";
                }
             
                //Send motion state
                mvmech.state = state;
            }
            Weapon_Pipe whandle = target.GetComponent (typeof(Weapon_Pipe)) as Weapon_Pipe;
            if (whandle != null) {
                //Handle shoot message
                if (Input.GetAxisRaw ("Mouse ScrollWheel") > 0) {
                    state ["add"] = "up";
                } else if (Input.GetAxisRaw ("Mouse ScrollWheel") < 0) {
                    state ["add"] = "down";
                } else {
                    state ["add"] = "None";
                }
                if (Input.GetMouseButton (1)) {
                    state ["shoot"] = "true";
                }

                //Send weapons state
                whandle.state = state;
            }

        }
        var camget = Camera.main;
        if (camget != null) {
            var chandle = camget.GetComponent (typeof(Camera_Pipe)) as Camera_Pipe;
            if (chandle != null) {
                state = chandle.state;
                //Handle camera view mode
                if (Input.GetKey (KeyCode.C)) {
                    state ["follow"] = "bird";
                } else if (Input.GetKey (KeyCode.X)) {
                    state ["follow"] = "normal";
                } else if (Input.GetKey (KeyCode.V)) {
                    state ["follow"] = "travel";
                } else if (Input.GetKey (KeyCode.F)) {
                    state ["follow"] = "HUD";
                }
                //Handle camera turning
                if (Input.GetKey (KeyCode.R)) {
                    state ["turn"] = "left";
                } else if (Input.GetKey (KeyCode.G)) {
                    state ["turn"] = "right";
                } else {
                    state ["turn"] = "None";
                }
                //Handle mouse tracking
                var ceen=Camera.main.GetComponent (typeof(Camera_Screen)) as Camera_Screen;
                var mpt = Camera.main.ScreenToViewportPoint (Input.mousePosition);
                var leng = ceen.leng;
                var tip = ceen.tip;
                if (mpt.x <= tip.x - leng.x) {
                    state ["trackx"] = "left";
                } else if (mpt.x >= tip.x + leng.x) {
                    state ["trackx"] = "right";
                } else {
                    state ["trackx"] = "None";
                }
                if (mpt.y <= tip.y - leng.y) {
                    state ["tracky"] = "up";
                } else if (mpt.y >= tip.y + leng.y) {
                    state ["tracky"] = "down";
                } else {
                    state ["tracky"] = "None";
                }
                 
                //Send follow state
                chandle.state = state;
            }
        }
    }
}
