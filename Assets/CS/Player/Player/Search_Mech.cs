﻿using UnityEngine;
using System.Collections;
using System.Linq;

public class Search_Mech : Photon.MonoBehaviour {
	//Search for the player.
	
	void Start () {
	}
	
	void Update () {
		AssignView_Handle ahandle = (AssignView_Handle)GameObject.FindObjectOfType(typeof(AssignView_Handle));
		if (GameObject.FindGameObjectWithTag("Player") && this.transform.root.gameObject.tag !="Player"){
			//Priority first on object tagged "Player".
			this.transform.parent = GameObject.FindGameObjectWithTag("Player").transform;
			GameObject[] go = GameObject.FindGameObjectsWithTag("Player");
			//Overide if needed
			if (PhotonNetwork.connected){
				foreach(GameObject g in go){
					//Check if owner is us. is mine
					if (g!= null && g.GetPhotonView() && g.GetPhotonView().owner.name==ahandle.owner){
						this.transform.parent=g.transform;
					}
				}}
			}}
}
