using UnityEngine;
using System.Collections;

public class Motion_Gear : MonoBehaviour {
    //Enables motion of the player.

    public Hashtable state = new Hashtable ();
    public float norspeed = 25f;
    public float walktorun=1.28f;
    public float walktoback=0.24f;
    public float force = 5;
    public bool on_the_ground = false;
    public string floor = "Leg"; //This needs to be the tag of the component touching the floor.
    private Timer_Util record;
    private float maxspeed;

    void Start () {
        ShowTrail (false);
        record = new Timer_Util ();
        state ["motion"] = "None";
        state ["jump"] = "None";
        maxspeed=norspeed*walktorun;
    }

    void Update () {
        Rigidbody SR = this.gameObject.rigidbody;
        float front = 0f;
        //Turning code
        if ((string)state ["turn"] == "left") {
            SR.drag = 3;
            SR.transform.RotateAround (SR.transform.position, Vector3.up, -100 * Time.deltaTime);
        } else if (state ["turn"] as string == "right") {
            SR.drag = 3;
            SR.transform.RotateAround (SR.transform.position, Vector3.up, 100 * Time.deltaTime);
        }
        //Walking,running,braking code
        SR.drag = 7;
        if ((string)state ["motion"] == "forward") {
            Vector3 angle = Quaternion.Euler (0, SR.transform.eulerAngles.y, 0) * Vector3.forward;
            if (SR.velocity.magnitude < maxspeed) {
                SR.drag = 1;
                //Switches between running and walking
                if ((string)state ["shift"] == "true") {
                    front = force * 5;
                    maxspeed=norspeed*walktorun;
                    ShowTrail (true); //Only allow trails when running continuously.
                } else {
                    front = force;
                    maxspeed=norspeed;
                    ShowTrail (false);
                }
                SR.AddForce (angle * front);
            } else {
                SR.velocity = maxspeed * angle;
            }
        } else if ((string)state ["motion"] == "back") {
            Vector3 angle = Quaternion.Euler (0, SR.transform.eulerAngles.y, 0) * Vector3.back;
            if (SR.velocity.magnitude < maxspeed) {
                SR.drag = 4;
                maxspeed=norspeed*walktoback;
                ShowTrail (false);
                SR.AddForce (angle * force);
            } else {
                SR.velocity = maxspeed * angle;
            }
        } else {
            ShowTrail (false);
        }
        if (on_the_ground) {
            //Jumping and quicksteps code.
            if ((string)state ["jump"] == "front") {
                Vector3 angle = Quaternion.Euler (0, SR.transform.eulerAngles.y, 0) * Vector3.forward;
                SR.AddForce (Vector3.up * force * 2f + angle * force * 2f);
            } else if ((string)state ["jump"] == "left") {
                Vector3 angle = Quaternion.Euler (0, SR.transform.eulerAngles.y, 0) * Vector3.left;
                SR.AddForce (Vector3.up * force * 1f + angle * force * 10f);
            } else if ((string)state ["jump"] == "right") {
                Vector3 angle = Quaternion.Euler (0, SR.transform.eulerAngles.y, 0) * Vector3.right;
                SR.AddForce (Vector3.up * force * 1f + angle * force * 10f);
            }
         
        }
        //Returns to idle state.
        SR.constraints = RigidbodyConstraints.FreezeRotation;
        if ((string)state ["motion"] == "None" && (string)state ["jump"] == "None") {
            SR.constraints |= RigidbodyConstraints.FreezePositionX | RigidbodyConstraints.FreezePositionZ;
        }
        FallFaster ();//Hack to fall faster on request.
        Animator ();
    }

    void Animator () {
        //State is processed directly for instant changes.
        string anime = "idle";
        if ((string)state ["motion"] == "forward") {
            if ((string)state ["shift"] == "true") {
                anime = "run";
            } else {
                anime = "walk";
            }
        } else if ((string)state ["motion"] == "back") {
            anime = "walk";
        }
        if ((string)state ["jump"] == "front") {
            anime = "jump";
        } else if ((string)state ["jump"] == "left") {
            anime = "quickstepL";
        } else if ((string)state ["jump"] == "right") {
            anime = "quickstepR";
        }
        if ((string)state ["motion"] == "None" && (string)state ["jump"] == "None") {
            anime = "idle";
        }
        StartCoroutine (playQ (anime));
    }

    void OnTheGround (Collision collision, string tag, bool state) {
        foreach (ContactPoint contact in collision.contacts) {
            if (contact.thisCollider.CompareTag (tag) || contact.otherCollider.CompareTag (tag)) {
                //Check for vertical collision, allow error of less than 12 degrees.
                if (Mathf.Abs (Vector3.Dot (this.transform.up, contact.normal)) >= 0.2) {
                    on_the_ground = state;
                }
            }
        }
    }

    public void ShowTrail (bool state) {
        foreach (TrailRenderer trail in this.gameObject.GetComponentsInChildren(typeof(TrailRenderer))) {
            trail.enabled = state;
        }
    }
             
    IEnumerator playQ (string anime) {
        if (this.animation [anime] != null) {
            this.animation.Play (anime);
            yield return new WaitForSeconds(animation [anime].length);
        }
    }
 
    void FallFaster () {
        Vector3 gravity = new Vector3 (0.0f, -9.8f, 0.0f);
        if (this.rigidbody.velocity.y < 0) {
            Physics.gravity = gravity * 4;
        } else {
            Physics.gravity = gravity;
        }
    }

    void OnCollisionEnter (Collision collision) {
        OnTheGround (collision, floor, true);
    }

    void OnCollisionExit (Collision collision) {
        OnTheGround (collision, floor, false);
    }
}