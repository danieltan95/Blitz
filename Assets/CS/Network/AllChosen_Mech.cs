﻿using UnityEngine;
using System.Collections;

public class AllChosen_Mech : Photon.MonoBehaviour {
		
	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
		var allchosen = true;
		GameObject[] con = (GameObject[])GameObject.FindGameObjectsWithTag("Wasp");
		if(con.Length>0){
			foreach(GameObject c in con){
				var d = (Confirm_Mech)c.GetComponent(typeof(Confirm_Mech));
				if (!d.chosen){
					allchosen=false;
				}
			}
			this.gameObject.SetActive(!allchosen);
		}
	}
}
