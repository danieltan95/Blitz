﻿using UnityEngine;
using System.Collections;
using System.Linq;

public class AssignView_Handle : Photon.MonoBehaviour {
    //Assign a wasp to synchronise everyone's position
 
    public GameObject wasp;
    public string owner;
    public bool allchosen = true;
    public bool offline = false;
    private bool flip = true;

    // Use this for initialization
    void Start () {
        if (!PhotonNetwork.connected) {
            PhotonNetwork.ConnectUsingSettings ("1.0");
        }
    }
 
    void OnDestroy () {
        PhotonNetwork.LeaveRoom ();
        PhotonNetwork.LeaveLobby ();
        PhotonNetwork.Disconnect ();
        print ("Disconnected");
    }
 
    void Update () {
        PhotonNetwork.offlineMode = offline;
        if (PhotonNetwork.connected && flip) {
            flip = false;
            var pwasp = wasp;
            //Instatiate a wasp to handle syncing and sharing of data.
            GameObject L = PhotonNetwork.Instantiate (pwasp.name, this.transform.position, Quaternion.identity, 0);
            owner = L.GetPhotonView ().owner.name;
        }
    }
 
    void OnJoinedLobby () {
        PhotonNetwork.JoinRandomRoom ();
    }
 
    void OnPhotonRandomJoinFailed () {
        Debug.Log ("Can't join random room!");
        PhotonNetwork.CreateRoom (null);
    }

    void OnLeftLobby () {
        Debug.Log ("We left the lobby.");
    }

    void OnLeftRoom () {
        Debug.Log ("This client has left a game room.");
    }

}
