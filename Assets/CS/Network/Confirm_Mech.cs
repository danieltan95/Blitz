﻿using UnityEngine;
using System.Collections;

public class Confirm_Mech : Photon.MonoBehaviour {
	
	public bool chosen = true;
	public int count = 0;
	
	private bool flip =false;

	void Start () {
	
	}
	
	void Update () {
		GameObject go = (GameObject)GameObject.FindGameObjectWithTag("Player");
		if (go!=null && go.name!="Player" && !flip){
			flip=true;
			this.photonView.RPC("Counter", PhotonTargets.All);
		}
		if (count == PhotonNetwork.playerList.Length){
			//Counts if everyone is on board
			chosen=true;
		}
	}
	
	[RPC]
    public void Counter(PhotonMessageInfo mi)
    {
		//adds a count to everyone that you're ready
        count+=1;
    }
}
