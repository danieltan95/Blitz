﻿using UnityEngine;
using System.Collections;

public class Port_Mech : MonoBehaviour {
	//Multiplayer warp portal, added network support.
	
	private GameObject[] exitlist;
	private bool exit =false;
	private bool flip = false;
	private AssignView_Handle ahandle;

	void Start () {
		exitlist=GameObject.FindGameObjectsWithTag("Exit");
	}

	void Update(){
		//Find all non-owned objects and disable them.
		ahandle = (AssignView_Handle)GameObject.FindObjectOfType(typeof(AssignView_Handle));
		if(exit){
			Search_Mech[] sme = (Search_Mech[])GameObject.FindObjectsOfType(typeof(Search_Mech));
			foreach(Search_Mech s in sme){
				var sname= s.gameObject.transform.root.gameObject.GetPhotonView();
				if(sname){
					if(sname.owner.name != ahandle.owner){	
						s.gameObject.SetActive(false);
					}
				}
			}
		}		
	}
	
	void OnTriggerExit (Collider other) {
		int index = Random.Range(0, exitlist.Length);
		var OR = other.gameObject.transform.root;
		if (OR.CompareTag("Player") && !flip ){
			if (OR.gameObject.name!="Player"){
				exit=true;
				flip=true;
				Vector3 pos=exitlist[index].transform.position;
				GameObject L= PhotonNetwork.Instantiate(OR.name,pos,Quaternion.identity,0);
				//Change the camera and main object to the instantiated zoid
                //Exception based code requires some subtle attention to details.
                //The order for the camera must follow the below.
                //Revert state to normal.
                (Camera.main.GetComponent(typeof(Camera_Pipe)) as Camera_Pipe).state["follow"]="normal";
                //Move the camera immediately
				Camera.main.transform.position=L.transform.position;
                //Parent the camera to the zoid.
				Camera.main.transform.parent=L.transform;
				OR.gameObject.transform.Find("Sky").parent=L.transform;
				OR.gameObject.SetActive(false);
			}
		}
	}
}
