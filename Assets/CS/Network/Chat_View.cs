﻿using System.Collections.Generic;
using UnityEngine;
using System.Collections;

[RequireComponent(typeof(PhotonView))]
public class Chat_View : Photon.MonoBehaviour {

    public Rect GuiRect = new Rect(0,0, 250,300);
    public bool IsVisible = true;
    public bool AlignBottom = true;
    public List<string> messages = new List<string>();
	
    private string inputLine = "";
    private Vector2 scrollPos = Vector2.zero;
	private int areavisible =0;
	private string areavisible_str="Hide";
	private bool joined_room =false;
	private bool leave_room = false;
	

    public static readonly string ChatRPC = "Chat";

    public void Start(){
        if (this.AlignBottom==true)
        {
			this.GuiRect.x = Screen.width - this.GuiRect.width-10;
			this.GuiRect.yMax=Screen.height/2;
            this.GuiRect.y = Screen.height - this.GuiRect.height-10;
        }
    }

    public void OnGUI()
    {
        if (!this.IsVisible || PhotonNetwork.connectionStateDetailed != PeerState.Joined)
        {
            return;
        }
        
        if (Event.current.type == EventType.KeyDown && (Event.current.keyCode == KeyCode.KeypadEnter || Event.current.keyCode == KeyCode.Return))
        {
            if (!string.IsNullOrEmpty(this.inputLine))
            {
				this.inputLine+="    "+System.DateTime.Now.ToShortTimeString();
                this.photonView.RPC("Chat", PhotonTargets.All, this.inputLine);
                this.inputLine = "";
                GUI.FocusControl("");
                return; // printing the now modified list would result in an error. to avoid this, we just skip this single frame
            }
            else
            {
                GUI.FocusControl("ChatInput");
            }
        }

        GUI.SetNextControlName("");
        GUILayout.BeginArea(this.GuiRect);
		
		
        scrollPos = GUILayout.BeginScrollView(scrollPos);
        GUILayout.FlexibleSpace();
		if (areavisible%2==0){
	        for (int i = messages.Count-1; i >=0 ; i--)
	        {
	            GUILayout.Label(messages[i]);
	        }
		}
        GUILayout.EndScrollView();

        GUILayout.BeginHorizontal();
        GUI.SetNextControlName("ChatInput");
		if (joined_room==true)
		{
			inputLine="entered the room "+"    "+System.DateTime.Now.ToShortTimeString();			
			this.photonView.RPC("Chat", PhotonTargets.All, this.inputLine);
            this.inputLine = "";
            GUI.FocusControl("");
			joined_room=false;
		}
		if (leave_room==true)
		{
			inputLine="left the room "+"    "+System.DateTime.Now.ToShortTimeString();			
			this.photonView.RPC("Chat", PhotonTargets.All, this.inputLine);
            this.inputLine = "";
            GUI.FocusControl("");
			leave_room=false;
		}
        inputLine = GUILayout.TextField(inputLine);
        if (GUILayout.Button("Send", GUILayout.ExpandWidth(false)))
        {
			this.inputLine+="    "+System.DateTime.Now.ToShortTimeString();
            this.photonView.RPC("Chat", PhotonTargets.All, this.inputLine);
            this.inputLine = "";
            GUI.FocusControl("");
        }
		if (GUILayout.Button(areavisible_str, GUILayout.ExpandWidth(false)))
		{
			areavisible+=1;
			if(areavisible%2 ==0)
			{
				areavisible_str="Hide";
			}
			else
			{
				areavisible_str="Show";
			}
		}
        GUILayout.EndHorizontal();
        GUILayout.EndArea();
    }

    [RPC]
    public void Chat(string newLine, PhotonMessageInfo mi)
    {
        string senderName = "anonymous";

        if (mi != null && mi.sender != null)
        {
            if (!string.IsNullOrEmpty(mi.sender.name))
            {
                senderName = mi.sender.name;
            }
            else
            {
                senderName = "player " + mi.sender.ID;
            }
        }

        this.messages.Add(senderName +": " + newLine);
    }

    public void AddLine(string newLine)
    {
        this.messages.Add(newLine);
    }
	
	private void OnJoinedRoom()
    {
        joined_room=true;
    }
	private void OnLeftRoom()
    {
        leave_room=true;
    }
}
