﻿using UnityEngine;
using System.Collections;

public class Room_View : Photon.MonoBehaviour {
	//GUI for the lobby room
	
	public string[] map_list = {"Jade_Garden_Map",};
	
    private bool connectFailed = false;
	private string roomName = "myRoom";
	private string mapname="Jade_Garden_Map";
    private Vector2 scrolllob = Vector2.zero;
	private Vector2 scrollpos = Vector2.zero;

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}
	
	private void OnGUI(){
        if (!PhotonNetwork.connected){
            PhotonNetwork.ConnectUsingSettings("1.0");
            GUI_Disconnected();
        }
        else{
            if (PhotonNetwork.room != null){
                GUI_Connected_Room();
            }
            else{
                GUI_Connected_Lobby();
            }       
        }
    }
	void GUI_Disconnected(){
        GUILayout.Label("Connection state: . (" + PhotonNetwork.connectionState);
		
        if (this.connectFailed){
            GUILayout.Label("Connection failed. Check setup and use Setup Wizard to fix configuration.");
            GUILayout.Label(string.Format("Server: {0}:{1}", PhotonNetwork.PhotonServerSettings.ServerAddress, PhotonNetwork.PhotonServerSettings.ServerPort));

            if (GUILayout.Button("Try Again", GUILayout.Width(100))){
                this.connectFailed = false;
                PhotonNetwork.ConnectUsingSettings("1.0");
            }
        }
    }
	
	void GUI_Connected_Lobby(){
        GUILayout.BeginArea(new Rect((Screen.width - 400) / 2, (Screen.height - 300) / 2, 400, 300));

        GUILayout.Label("Main Menu");

        // Player name
        GUILayout.BeginHorizontal();
        GUILayout.Label("Player name:", GUILayout.Width(150));
        PhotonNetwork.playerName = GUILayout.TextField(PhotonNetwork.playerName);
        if (GUI.changed){
            PlayerPrefs.SetString("playerName" + Application.platform, PhotonNetwork.playerName);
        }

        GUILayout.EndHorizontal();
        GUILayout.Space(15);


        // Join room by title
        GUILayout.BeginHorizontal();
        GUILayout.Label("JOIN ROOM:", GUILayout.Width(150));
        this.roomName = GUILayout.TextField(this.roomName);
        if (GUILayout.Button("GO")){
            PhotonNetwork.JoinRoom(this.roomName);
        }
        GUILayout.EndHorizontal();


        // Create a room (fails if already exists!)
        GUILayout.BeginHorizontal();
        GUILayout.Label("CREATE ROOM:", GUILayout.Width(150));
        this.roomName = GUILayout.TextField(this.roomName);
        if (GUILayout.Button("GO")){
            PhotonNetwork.CreateRoom(this.roomName, true, true, 10);
        }
        GUILayout.EndHorizontal();
		
		GUILayout.BeginVertical();
		GUILayout.Label("Choose map to play: ");
		GUILayout.Label("Map list");
		scrollpos=GUILayout.BeginScrollView(scrollpos);
		foreach (string Map in  map_list){
			if (GUILayout.Button(Map)){
				mapname=Map;
			}
		}
		GUILayout.EndScrollView();
		GUILayout.Label("Current map: "+mapname);
		GUILayout.EndVertical();

        // Join random room (there must be at least 1 room)
        GUILayout.BeginHorizontal();
        GUILayout.Label("JOIN RANDOM ROOM:", GUILayout.Width(150));
        if (PhotonNetwork.GetRoomList().Length > 0 && GUILayout.Button("GO")){
                PhotonNetwork.JoinRandomRoom();
        }
        else{
            GUILayout.Label("..no games available...");            
        }
        GUILayout.EndHorizontal();
        GUILayout.Space(30);

        //Show a list of all current rooms
        GUILayout.Label("ROOM LISTING:");
        if (PhotonNetwork.GetRoomList().Length == 0)
        {
            GUILayout.Label("..no games available..");
        }
        else
        {
            // Room listing: simply call GetRoomList: no need to fetch/poll whatever!
            this.scrolllob = GUILayout.BeginScrollView(this.scrolllob);
            foreach (RoomInfo game in PhotonNetwork.GetRoomList())
            {
                GUILayout.BeginHorizontal();
                GUILayout.Label(game.name + " " + game.playerCount + "/" + game.maxPlayers);
                if (GUILayout.Button("JOIN"))
                {
                    PhotonNetwork.JoinRoom(game.name);
                }

                GUILayout.EndHorizontal();
            }

            GUILayout.EndScrollView();
        }

        GUILayout.EndArea();
    }
	
	void GUI_Connected_Room()
    {
        GUILayout.Label("We are connected to room: "+PhotonNetwork.room);
        GUILayout.Label("Players: ");
        foreach (PhotonPlayer player in PhotonNetwork.playerList)
        {
            GUILayout.Label("ID: "+player.ID+" Name: "+player.name);
        }

        if (GUILayout.Button("Leave room"))
        {
            PhotonNetwork.LeaveRoom();
        }
    }

	
	// We have two options here: we either joined(by title, list or random) or created a room.
    private void OnJoinedRoom(){
        Debug.Log("We have joined a room.");
        StartCoroutine(MoveToGameScene());
    }

    private void OnCreatedRoom(){
        Debug.Log("We have created a room.");
        //When creating a room, OnJoinedRoom is also called, so we don't have to do anything here.
    }


    private void OnFailedToConnectToPhoton(object parameters){
        this.connectFailed = true;
        Debug.Log("OnFailedToConnectToPhoton. StatusCode: " + parameters);
    }
	
	private IEnumerator MoveToGameScene(){
        //Wait for the 
        while (PhotonNetwork.room == null)
        {
            yield return 0;
        }

        Debug.LogWarning("Normally we would load the game scene right now.");
		PhotonNetwork.LoadLevel("Middle_Map");
		Application.LoadLevelAdditive(mapname);
    }

}
