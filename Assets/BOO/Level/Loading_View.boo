﻿import UnityEngine

class Loading_View (MonoBehaviour):
	#Provides the loading screen
	#NGUI port later
	
	public Loading_Screen as Texture
	
	private loading as int =69
	
	def Start():
		GameObject.DontDestroyOnLoad(self.gameObject)
	
	def OnGUI ():
		if Application.isLoadingLevel:
			sw=Screen.width
			sh=Screen.height
			w=Loading_Screen.width*0.8
			h=Loading_Screen.height*0.8
			GUI.Box(Rect(0,0,sw,sh),'')
			GUI.Box(Rect(sw*0.5-w*0.5,sh*0.5-h*0.5,w,h),Loading_Screen)
			GUI.Label(Rect(Screen.width/2-10 , Screen.height*3/4, 200, 100),loading.ToString()+" %")
			loading+=Random.Range(10,30)
		else: loading =69