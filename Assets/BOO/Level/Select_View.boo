﻿import UnityEngine

class Select_View (MonoBehaviour): 
	#Provides the map selection
	#NGUI port later
	
	public map_list as (string)
	public texture as (Texture)
	public speed=10 
	public leng =120

	private scrollpos as Vector2 = Vector2.zero	
	private start as int
	private end as int
	
	def Start ():
		start=-leng*1.5
		end=Screen.width/2-leng
	
	def Update ():
		#Shifts the area horizontally
		if start<end:
			start+=speed
		
	def OnGUI():
		GUI.skin.button.fontSize=14
		GUILayout.BeginArea(Rect(start,25,leng*2,300))
		GUILayout.BeginVertical()
		GUILayout.Label("Choose map to play: ")
		GUILayout.Label("Map list")
		if start>=end:
			scrollpos=GUILayout.BeginScrollView(scrollpos)
			for Map as string,Tex as Texture in zip(map_list,texture) :
				if GUILayout.Button(GUIContent(Map,Tex)):
					Application.LoadLevel("Loading_Map")
					Application.LoadLevelAdditive(Map)
			GUILayout.EndScrollView()
		GUILayout.EndVertical()
		GUILayout.EndArea()
		