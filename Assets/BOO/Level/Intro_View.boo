﻿import UnityEngine

class Intro_View (MonoBehaviour): 
	#Introduction page view
	#NGUI port later
	
	public Introtext="This is a hacked together preview build.\n"+\
	          "W-Moves forward, press shift to run\n"+\
	          "S-Brake\n"+\
	          "A,D-Moves left or right\n"+\
	          "Q/E-Jumps left or right\n"+\
	          "Space-Jumps up\n"+\
	          "C-Switch to first person view, use mouse to rotate camera\n"+\
	          "V-Switch to travel view, press R/G to rotate camera\n"+\
	          "F-Switch to weapon view, circles are enemies\n"+\
	          "X-Switch to normal view\n"+\
	          "Shoot with right mouse button\n"+\
	          "Switch weapons with scroll button\n"

	def Start ():
		pass
	
	def Update ():
		pass
	
	def OnGUI():
		GUI.skin.label.fontSize=14
		wt=Screen.width/2
		leng=300
		
		#Show text
		rec=Rect(wt-leng/2,25,leng,300)
		GUI.Box(rec,'')
		GUILayout.BeginArea(rec)
		GUILayout.Label(Introtext)
		if GUILayout.Button("Back"):
			Application.LoadLevel("Start_Level")
		GUILayout.EndArea()
