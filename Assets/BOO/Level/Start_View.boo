﻿import UnityEngine

class Start_View (MonoBehaviour): 
	#Starting page GUI
	#NGUI port later
	
	public startpage as Texture
	
	private modechoose as bool=false
	
	def OnGUI() as void :
		ht=Screen.height/2
		wt=Screen.width/2
		w=startpage.width*0.75
		h=startpage.height*0.8
		rec=Rect(wt-70,ht-140,140,120)
		if GUI.Button(rec,"Start"):
			modechoose = not modechoose
		rec=Rect(wt-w*0.5,ht*0.95,w*0.25,90)
		if GUI.Button(rec,"Intro"):
			Application.LoadLevel("Controls_Level")
		rec=Rect(wt+w*0.25,ht*0.95,w*0.25,90)
		if GUI.Button(rec,"Exit"):
			Application.Quit()
		#Cover the buttons.
		rec=Rect(wt-w*0.5,ht-h*0.5,w,h)
		GUI.Label(rec,startpage)
		
		if modechoose:
			leng=100
			rec=Rect(wt-leng*0.5,ht+50,leng,60)
			GUI.Box(rec,'')
			GUI.Box(rec,'')
			rec=Rect(wt-leng*0.5,ht+50,leng,30)
			if GUI.Button(rec,"Single"):
				Application.LoadLevel("Selection_Level")
			rec=Rect(wt-leng*0.5,ht+80,leng,30)
			if GUI.Button(rec,"Multi"):
				Application.LoadLevel("Room_Level")