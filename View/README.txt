Blitz
====

MMORPG 
------

* Description: 
	* *Windows* only MMORPG using similar idea as ZOIDS.
	* The game is being rewritten for various reasons, most importantly consistency and scalability.
	* The current preview build for the dev team is in Out\View.zip
	* To play the game, just double click the unity file. Never delete the ZOIDDATA folder.

[For anyone who has met with suggestions and bug reports](http://zoidsmmorpg.freeforums.org/index.php)

**Please read the INTRO before playing if not familiar with in game actions.**

Requirements:
-------------

* windows x32/x86/x64 machine.
* Internet access for the multiplayer.

##Zoid controls:

###Keyboard

1. W     - Move forward, press left/right shift to run.
2. A/D   - Turns to left or right.
3. S     - Brakes.
4. Q/E   - Jump left or right.
5. Space - Jumps up.
6. X     - Normal camera view.
7. C     - First person view, use mouse to rotate camera.
8. V     - Travel view, use R/G to rotate camera.
9. F     - Weapon view, circles are enemies

###Mouse

1. Scroll up/down - Switch weapons clockwise/counter-clockwise
2. Left           - Shoot weapon

##Links

[Dev Blog    ](https://www.shadowys.github.io)

[Website     ](https://www.shadowys.github.io/Zoids-MMORPG)

[GitHub Repo ](https://www.github.com/Shadowys/Blitz)

[Forums      ](http://www.zoidsmmorpg.freeforums.org)

[Issue Reports](https://www.github.com/Shadowys/Blitz/issues)

Participate by registering your name in the forums below and PM me, [Shadowys](http://www.zoidsmmorpg.freeforums.org/memberlist.php?mode=viewprofile&u=62) or [a6yog](http://www.zoidsmmorpg.freeforums.org/memberlist.php?mode=viewprofile&u=2).